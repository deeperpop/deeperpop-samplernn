#!/usr/bin/env bash
# Initialize the venv
python3 -m venv --system-site-packages --without-pip .

# Enter the venv
source ./bin/activate

# Install pip within the venv
wget https://bootstrap.pypa.io/get-pip.py
python get-pip.py

# Install PyTorch
pip install --upgrade http://download.pytorch.org/whl/cu80/torch-0.2.0.post3-cp35-cp35m-manylinux1_x86_64.whl
pip install torchvision

# Install samplernn-pytorch dependencies
pip install --upgrade -r samplernn-pytorch/requirements.txt

# Install comet_ml
pip install --upgrade comet_ml

# Install pandas
pip install --upgrade pandas

# Install matplotlib
pip install --upgrade matplotlib

# Install seaborn
pip install --upgrade seaborn

# Deactivate the virtual environment
deactivate
