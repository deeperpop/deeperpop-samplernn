#!/usr/bin/env sh
python ../samplernn-pytorch/train.py \
  --exp stretched-twotier-popular85 \
  --comet_key Lc8fBN4vkZhZnahCmO2I0TaEU \
  --datasets_path /farmshare/user_data/connorb3/deeperpop/data/wav/ \
  --dataset stretched-sliced-popular85 \
  --batch_size 32 \
  --frame_sizes 16 16 \
  --n_rnn 3 \
  --n_samples 10 \
  --sample_length 160000 \
  --results_path /farmshare/user_data/connorb3/deeperpop/model/srnn-stretched-twotier-popular85
